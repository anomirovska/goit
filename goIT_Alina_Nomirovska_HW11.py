from collections import UserDict
import datetime
import re



class Field:

    def __init__(self, value):
        self.__value = None
        self.value = value

    @property
    def value(self):
        return self.__value

    @value.setter
    def value(self, new_value):
        self.__value = new_value



class Name(Field):
    pass



class Phone(Field):
    
    def __init__(self, phone):
        self.__value = None
        self.value = phone
    
    @property
    def phone(self):
        return self.__value

    @phone.setter
    def phone(self, new_value):
        try:
            if re.match(r'\+380\d{9}', new_value):
                self.__value = new_value
            else:
                raise ValueError('Phone should be in a format +380XXXXXXXXX. Please, try again!')
        except:
            raise ValueError('Please, enter a string!')



class Birthday:

    def __init__(self, birthday):
        self.__birthday = None
        self.birthday = birthday
    
    @property
    def birthday(self):
        return self.__birthday

    @birthday.setter
    def birthday(self, new_value):
        new_year = int(new_value[:4])
        new_month = int(new_value[5:7])
        new_day = int(new_value[-2:])
        try:
            datetime(year=new_year, month=new_month, day = new_day)
            self.__birthday = new_value
        except:
            raise ValueError('Birthday should be in a format YYYY-MM-DD. Please, try again!')



class Record():

    def __init__(self, name: Name, phone: Phone = None, birthday: Birthday = None):
        self.name = name
        self.birthday = birthday
        self.phones = []
        if isinstance(phone, Phone):
            self.phones.append(phone)

    def add(self, phone):
        self.phones.append(phone)

    def edit(self, phone):
        self.phones = phone

    def delete(self, phone):
        self.phones.remove(phone)

    def days_to_birthday(self, birthday):
        #birthday is a string of a format 'yyyy-mm-dd'
        current_date = datetime.now()
        current_year = current_date.year
        birthday = datetime.strptime(birthday, '%Y-%m-%d')
        new_birthday = birthday.replace(year=current_year)

        if new_birthday > current_date:
            return f'{(new_birthday-current_date).days}'

        else:
            your_birthday = new_birthday.replace(year=current_year+1)
            return f'{(your_birthday- current_year).days}'



class AddressBook(UserDict):

    def add_record(self, record):
        self.data[record.name.value] = record

    def generator(self):
        for name, phone in self.data.items():
            yield f'{name}: {phone}'

    def iterator(self, n):
        if n > len(self.data):
                print('Your value is bigger than the number of contacts.')
        else:
            while n > 0:
                try:
                    print(next(self.generator()))
                    nhep -= 1
                except StopIteration:
                    print('Your value is bigger than the number of contacts.')
                    return ""



phone_dict = {}

def input_error(func):

    def inner(*args):

        try: 
            func(*args)
        except IndexError:
            print('Your input should be in the following order: /{command/} /{Name/} /{Phone Number/}')
            func(*args)
        except KeyError:
            print('Either the command or the name is wrong, please try again!')
            func(*args)
        except ValueError:
            print('Please try again!')
            func(*args)
            
    return inner


def hello(*args):
    return 'How can I help you?'


def add(input_parsed):
    name = input_parsed[-2]
    phone = input_parsed[-1]
    phone_dict[name] = phone
    return 'Done!'


def change(input_parsed):
    name = input_parsed[-2]
    phone = input_parsed[-1]
    #check if raises KeyError
    phone_dict[name]
    phone_dict[name] = phone
    return 'Done!'


def phone(input_parsed):
    name = input_parsed[-1]
    return phone_dict[name]


def show_all(*args):
    return f'You phone book is {phone_dict}'


def close(*args):
    return 'Good bye!'


HANDLER_DICT = {'hello': hello, 'add': add, 'change': change, 'phone': phone, 'show': show_all}


def handler(command_name):
    return HANDLER_DICT[command_name]


@input_error
def main():


    while True:
        user_input = input("Enter command >>> ")
        input_parsed = user_input.split()
        command = input_parsed[0].lower()

        if user_input in ('good bye', 'close', 'exit'):
            close()
            break

        if command not in HANDLER_DICT.keys():
            raise ValueError

        print(handler(command)(input_parsed))


if __name__ == '__main__':
    main()