from collections import defaultdict
from datetime import datetime, timedelta
#input: list of dictionaries with keys "name" and "birthday"
#outup: print user list with their birthdays

def get_birthdays_per_week (birthday_list):
    current_date = datetime.now().date()
    grouped = defaultdict(list)

    for item in birthday_list:
        birthday = item['birthday']
        name = item['name']
        #convert to datetime object
        birthday = datetime.strptime(birthday, '%Y-%m-%d %H:%M:%S')
        birthday = birthday.replace(year=current_date.year)

        if current_date + timedelta(days=7) > birthday.date():
            #extract day of the week
            day = datetime.strftime(birthday, '%A')
            grouped['Monday'].append(name) if day in ('Saturday', 'Sunday') else grouped[day].append(name)
            
    [print(f'{key}: {value}') for key, value in grouped.items()]

def main():
    #test_input = [{'name': 'Mom', 'birthday': '1974-02-02 11:05:14'},{'name': 'Vika', 'birthday': '1998-02-05 12:11:10'}, {'name': 'Alina', 'birthday': '1997-06-15 17:53:00'}, {'name': 'Anna', 'birthday': '1999-06-22 23:25:11'}, {'name': 'Jean Paul', 'birthday': '1995-08-10 14:15:08'}]
    get_birthdays_per_week(user_input)

if __name__ == '__main__':
    main()