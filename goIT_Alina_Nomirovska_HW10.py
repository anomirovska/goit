from collections import UserDict


class Field:
    def __init__(self, value):
        self.value = value


class Name(Field):
    pass


class Phone(Field):
    pass


class Record():
    def __init__(self, name, phone=None):
        self.name = name
        self.phones = []
        if isinstance(phone, Phone):
            self.phones.append(phone)

    def add(self, phone):
        self.phones.append(phone)

    def edit(self, phone):
        self.phones = phone

    def delete(self, phone):
        self.phones.remove(phone)


class AddressBook(UserDict):
    def add_record(self, record):
        self.data[record.name.value] = record



phone_dict = {}

def input_error(func):

    def inner(*args):

        try: 
            func(*args)
        except IndexError:
            print('Your input should be in the following order: /{command/} /{Name/} /{Phone Number/}')
            func(*args)
        except KeyError:
            print('Either the command or the name is wrong, please try again!')
            func(*args)
        except ValueError:
            print('This command does not exist, please try again!')
            func(*args)
            
    return inner


def hello(*args):
    return 'How can I help you?'


def add(input_parsed):
    name = input_parsed[-2]
    phone = input_parsed[-1]
    phone_dict[name] = phone
    return 'Done!'


def change(input_parsed):
    name = input_parsed[-2]
    phone = input_parsed[-1]
    #check if raises KeyError
    phone_dict[name]
    phone_dict[name] = phone
    return 'Done!'


def phone(input_parsed):
    name = input_parsed[-1]
    return phone_dict[name]


def show_all(*args):
    return f'You phone book is {phone_dict}'


def close(*args):
    return 'Good bye!'


HANDLER_DICT = {'hello': hello, 'add': add, 'change': change, 'phone': phone, 'show': show_all}


def handler(command_name):
    return HANDLER_DICT[command_name]


@input_error
def main():

    while True:
        user_input = input("Enter command >>> ")
        input_parsed = user_input.split()
        command = input_parsed[0].lower()

        if user_input in ('good bye', 'close', 'exit'):
            close()
            break

        if command not in HANDLER_DICT.keys():
            raise ValueError

        print(handler(command)(input_parsed))

if __name__ == '__main__':
    main()