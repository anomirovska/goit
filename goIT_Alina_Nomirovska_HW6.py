import os
import pathlib
import re
import shutil
import sys


formats_dict = {'images' : ['jpeg', 'png', 'jpg', 'svg'], 'documents':['doc', 'docx', 'txt', 'pdf', 'xlsx', 'pptx'],
                    'video':['avi', 'mp4', 'mov', 'mkv'], 'audio':['mp3', 'ogg', 'wav', 'amr'], 
                    'archives':['zip', 'gz', 'tar']}

files_dict = {'images' : [], 'documents':[], 'video':[], 'audio':[], 'archives':[]}
met_formats = []
unknown_formats = []


def normalize(item_name):

    CYRILLIC_SYMBOLS = "абвгдеёжзийклмнопрстуфхцчшщъыьэюяєіїґ"
    TRANSLATION = ("a", "b", "v", "g", "d", "e", "e", "j", "z", "i", "j", "k", "l", "m", "n", "o", "p", "r", "s", "t", "u",
               "f", "h", "ts", "ch", "sh", "sch", "", "y", "", "e", "yu", "ya", "je", "i", "ji", "g")
    MAP = {}
    
    for c, t in zip(CYRILLIC_SYMBOLS, TRANSLATION):
        MAP[ord(c)] = t
        MAP[ord(c.upper())] = t.upper()

    suffix = os.path.splitext(item_name)[1]
    normalized_name = os.path.basename(item_name).replace(suffix, '').translate(MAP)
    extra_symbols = re.findall(r'[^a-zA-Z0-9]', normalized_name)

    for symbol in extra_symbols:
        normalized_name = normalized_name.replace(symbol, '_')

    return os.path.join(os.path.dirname(item_name), normalized_name+suffix)


#if an empty folder is not deleted there is probably an invisible file inside
def sorting(input, base_folder, formats=formats_dict , files=files_dict, met=met_formats, unknown=unknown_formats):
 
    for item in input.iterdir():
        os.rename(item, normalize(item))

    for item in input.iterdir():   
        if os.path.isfile(item):
            for key, value in formats.items():
                suffix = os.path.splitext(item)[1][1:]
                name = os.path.basename(item)

                if suffix in value:
                    new_folder = os.path.join(base_folder, key)
                    os.makedirs(new_folder, exist_ok=True)
                    shutil.move(item, os.path.join(new_folder, name))
                    files[key].append(name)
                    met.append(suffix)
                    continue

                if suffix in formats_dict['archives']:
                    zip_suffix = os.path.splitext(item)[1]
                    shutil.unpack_archive(item, os.path.join(base_folder, 'archives', name.replace(zip_suffix, '')))
                    continue

            if suffix not in met:
                unknown.append(suffix)

        if not os.listdir(input):
                os.rmdir(input)

        elif os.path.isdir(item):
            if not os.listdir(item):
                os.rmdir(item)
            elif os.listdir(item):
                sorting(item, base_folder)

    return set(met_formats), set(unknown_formats), files_dict


def main():

    user_input = pathlib.Path(sys.argv[1])
    sorting(input=user_input, base_folder=user_input)


if __name__ == "__main__":
    main()